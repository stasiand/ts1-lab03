package cz.cvut.fel.ts1.hw02;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
    @Test
    public void add_oneAddTwo_three(){

        // ARRANGE
        Calculator calculator = new Calculator();

        // ACT
        int result = calculator.add(1, 2);

        // ASSERT
        Assertions.assertEquals(3, result);

    }

    public void subtract_twoSubtractOne_one(){

            // ARRANGE
            Calculator calculator = new Calculator();

            // ACT
            int result = calculator.subtract(2, 1);

            // ASSERT
            Assertions.assertEquals(1, result);
    }

    public void multiply_twoMultiplyThree_six(){

            // ARRANGE
            Calculator calculator = new Calculator();

            // ACT
            int result = calculator.multiply(2, 3);

            // ASSERT
            Assertions.assertEquals(6, result);
    }

    public void divide_sixDivideTwo_three(){

            // ARRANGE
            Calculator calculator = new Calculator();

            // ACT
            int result = calculator.divide(6, 2);

            // ASSERT
            Assertions.assertEquals(3, result);
    }
    public void divide_divideByZero_throwsArithmeticException(){

            // ARRANGE
            Calculator calculator = new Calculator();

            // ACT + ASSERT
            Assertions.assertThrows(ArithmeticException.class, () -> calculator.divide(6, 0));
    }
}
